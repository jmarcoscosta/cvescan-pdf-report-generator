from export_table import export_single_table
from intermediate import export_intermediate_csv, get_all_packages
from bar_plot import plot_single
import csv
import sys

path = sys.argv[1]
rows_of_interest = []

def is_package_of_interest(row):
    vulnerable = row[5] == 'True'
    severity = row[7] in ['HIGH', 'MEDIUM', 'LOW']
    attack_vector_is_network = row[8] == 'NETWORK'

    if vulnerable and severity and attack_vector_is_network:
        return True

    return False

with open(path, 'r') as report:
    csvreader = csv.reader(report)
    header = next(csvreader)
    for row in csvreader:
        if is_package_of_interest(row):
            rows_of_interest.append(row)

with open("filtered_report.csv", 'w', newline="") as filtered_intermediate:
    csvwriter = csv.writer(filtered_intermediate)
    csvwriter.writerows(rows_of_interest)

packages = get_all_packages("filtered_report.csv")
export_intermediate_csv("filtered_report.csv", packages, "_filtered")
export_single_table("intermediate_filtered.csv")
plot_single("intermediate_filtered.csv", [], "filtered_diagram.pdf")
