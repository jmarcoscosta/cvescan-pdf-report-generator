"""
- packages_of_interest contains the packages that will be listed
- Leave packages_of_interest empty if you want to list all packages
- Leave exclude_pkgs_from_plot empty if you don't want to exclude any packages

Example:

packages_of_interest = {
    "linux" : Package("linux"),
    "perl" : Package("perl")
}

in this case, only linux and perl will be present in the table and bar plot.

Example:
exclude_pkgs_from_plot = ["linux"]

in this case, only linux will be excluded from the table and bar plot.
"""

from intermediate import export_intermediate_csv, get_all_packages
from export_table import export_single_table, export_old_new_table
from bar_plot import plot_single, plot_old_new
from command_line_parsing import parse_command_line

def main():
    packages_of_interest = {}
    exclude_pkgs_from_plot = []
    args, parser = parse_command_line()

    if not any(vars(args).values()):
        parser.error("No arguments were given.")

    if args.single:
        path_to_source_report = args.single[0]
        if not packages_of_interest:
            packages_of_interest = get_all_packages(path_to_source_report)
        export_intermediate_csv(path_to_source_report, packages_of_interest, "_single")

        if not args.intermediates_only:
            export_single_table("intermediate_single.csv")
            plot_single("intermediate_single.csv", exclude_pkgs_from_plot)
    elif args.old_new:
        old_report, new_report = args.old_new[0], args.old_new[1]
        # old and new reports may not have the same packages
        if not packages_of_interest:
            candidate1 = get_all_packages(old_report)
            candidate2 = get_all_packages(new_report)
            # choose the candidate with the most packages
            if len(candidate1) > len(candidate2):
                packages_of_interest = candidate1
            else:
                packages_of_interest = candidate2

        export_intermediate_csv(old_report, packages_of_interest, "_old")
        export_intermediate_csv(new_report, packages_of_interest, "_new")
        if not args.intermediates_only:
            export_old_new_table("intermediate_old.csv", "intermediate_new.csv")
            plot_old_new("intermediate_old.csv", "intermediate_new.csv", exclude_pkgs_from_plot)
    elif args.export_and_plot:
        option = args.export_and_plot
        if option == "single":
            export_single_table("intermediate_single.csv")
            plot_single("intermediate_single.csv", exclude_pkgs_from_plot)
        elif option == "both":
            export_old_new_table("intermediate_old.csv", "intermediate_new.csv")
            plot_old_new("intermediate_old.csv", "intermediate_new.csv", exclude_pkgs_from_plot)

if __name__ == "__main__":
    main()
