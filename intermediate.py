""" Generates an intermediate csv file with the packages of interest and its details.
The details are the amount of low, medium and high severity CVE entries, but only the vulnerable entries are counted. """
import csv

class Package:
    def __init__(self, _name):
        self.name = _name
        self.high_count = 0
        self.medium_count = 0
        self.low_count = 0

    # returns name and counts packed into a list
    def get_entry(self):
        return [self.name, self.high_count, self.medium_count, self.low_count, self.get_total()]

    def parse_csv_row(self, row):
        vulnerable = row[5]
        if vulnerable == 'True':
            severity = row[7]
            if severity == 'HIGH':
                self.high_count += 1
            elif severity == 'MEDIUM':
                self.medium_count += 1
            elif severity == 'LOW':
                self.low_count += 1

    def get_total(self):
        return self.high_count + self.medium_count + self.low_count

    def restart_count(self):
        self.high_count = 0
        self.medium_count = 0
        self.low_count = 0

def read_intermediate_single(intermediate_filename):
    packages = []
    with open(intermediate_filename, 'r') as source:
        csvreader = csv.reader(source)
        header = next(csvreader)
        for row in csvreader:
            packages.append(intermediate_row_to_package(row))

    return packages

def read_intermediate_old_new(intermediate_old_filename, intermediate_new_filename):
    old_packages = read_intermediate_single(intermediate_old_filename)
    new_packages = read_intermediate_single(intermediate_new_filename)

    return old_packages, new_packages

def remove_package_by_name(packages_list, name):
    for p in packages_list:
        if p.name == name:
            packages_list.remove(p)

def intermediate_row_to_package(row):
    package = Package(row[0])
    package.high_count = int(row[1])
    package.medium_count = int(row[2])
    package.low_count = int(row[3])

    return package

def get_all_packages(csv_filename):
    package_names = set()
    with open(csv_filename, 'r') as source:
        csvreader = csv.reader(source)
        header = next(csvreader)
        for row in csvreader:
            package_names.add(row[3])

    # return dictionary of package names and Package objects
    package_names = list(package_names)
    packages = {}
    for p in package_names:
        packages[p] = Package(p)

    return packages

def export_intermediate_csv(path_to_source_report, packages_of_interest, suffix = ""):
    rows = []
    intermediate_csv_entries = [['Package', 'H_count', 'M_count', 'L_count', 'total']]

    with open(path_to_source_report, 'r') as source:
        csvreader = csv.reader(source)
        for row in csvreader:
            package_name = row[3]
            if package_name in packages_of_interest:
                package = packages_of_interest[package_name]
                package.parse_csv_row(row)

    for package in packages_of_interest.values():
        intermediate_csv_entries.append(package.get_entry())
        package.restart_count()

    # sort entries list (list of lists, by their first elements)
    intermediate_csv_entries = sorted(intermediate_csv_entries, key = lambda x : x[0])

    with open("intermediate" + suffix + ".csv", 'w', newline="") as intermediate_file:
        csvwriter = csv.writer(intermediate_file)
        csvwriter.writerows(intermediate_csv_entries)
