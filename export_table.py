""" Exports .md file with LaTeX table. """
import csv

def get_colored_values(val1, val2):
    v1, v2 = int(val1), int(val2)
    ret1, ret2 = val1, val2

    if v1 != v2:
        if v1 > v2:
            ret1 = "\\textbf{"+val1+"}"
            ret2 = "\\textcolor{teal}{\\textbf{"+val2+"}}"
        else:
            ret1 = "\\textbf{"+val1+"}"
            ret2 = "\\textcolor{red}{\\textbf{"+val2+"}}"

    return ret1, ret2

def package_to_table_line(csv_row, csv_row2 = False):
    line = csv_row[0]
    if not csv_row2:
        for val in csv_row[1:4]:
            line += "\t\t & " + val
    else:
        for val_old, val_new in zip(csv_row[1:4], csv_row2[1:4]):
            # bold and colored values
            bc_old, bc_new = get_colored_values(val_old, val_new)
            line += "\t\t & " + bc_old
            line += "\t\t & " + bc_new

    line += "\\\\"

    return line

def total_count_to_line(total_count):
    line = "Total"
    for t in total_count:
        line += "\t\t& " + str(t)

    line += "\\\\"

    return line

def export_single_table(intermediate_filename):
    total_count = [0, 0, 0] # high, medium and low

    # table header
    table_lines_list = ["\\begin{tabularx}{\\textwidth}{XXXX}",
                        "\hline",
                        "Severity   & High   & Medium    & Low\\\\",
                        "\hline"]

    with open(intermediate_filename, 'r') as source:
        csvreader = csv.reader(source)
        header = next(csvreader)
        for row in csvreader:
            table_lines_list.append(package_to_table_line(row))
            for i in range(3):
                total_count[i] += int(row[i + 1])

    # total line
    table_lines_list.append("\hline")
    table_lines_list.append(total_count_to_line(total_count))

    # table bottom
    table_lines_list.append("\hline")
    table_lines_list.append("\end{tabularx}")

    with open("table.md", 'w') as table:
        for line in table_lines_list:
            table.write(line)
            table.write('\n')

def export_old_new_table(intermediate_old_filename, intermediate_new_filename):
    total_count_old = [0, 0, 0]
    total_count_new = [0, 0, 0]
    total_count = []

    # table header
    table_lines_list = ["\\begin{tabularx}{\\textwidth}{XXXXXXX}",
                        "\hline",
                        "Version & Old & New  & Old  & New & Old  & New\\\\",
                        "Severity & High & High & Medium & Medium & Low  & Low\\\\",
                        "\hline"]

    with open(intermediate_old_filename, 'r') as old, open(intermediate_new_filename, 'r') as new:
        csvreader_old, csvreader_new = csv.reader(old), csv.reader(new)
        header, header_ = next(csvreader_old), next(csvreader_new)
        for row_old, row_new in zip(csvreader_old, csvreader_new):
            table_lines_list.append(package_to_table_line(row_old, row_new))
            for i in range(3):
                total_count_old[i] += int(row_old[i + 1])
                total_count_new[i] += int(row_new[i + 1])

    # total line
    table_lines_list.append("\hline")
    for i in range(3):
        total_count.append(total_count_old[i])
        total_count.append(total_count_new[i])

    table_lines_list.append(total_count_to_line(total_count))

    # table bottom
    table_lines_list.append("\hline")
    table_lines_list.append("\end{tabularx}")

    with open("table.md", 'w') as table:
        for line in table_lines_list:
            table.write(line)
            table.write('\n')
