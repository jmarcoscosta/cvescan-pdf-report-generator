from matplotlib import pyplot as plt
from intermediate import intermediate_row_to_package
from intermediate import read_intermediate_single, read_intermediate_old_new
from intermediate import remove_package_by_name
import numpy
import csv

def plot_single(intermediate_filename, exclude_packages, figname = "diagram.pdf"):
    packages = read_intermediate_single(intermediate_filename)
    fig, ax = plt.subplots()

    if exclude_packages:
        for e in exclude_packages:
            remove_package_by_name(packages, e)

    # data
    package_names = [p.name for p in packages]
    high_counts = [p.high_count for p in packages]
    medium_counts = [p.medium_count for p in packages]
    low_counts = [p.low_count for p in packages]
    y_pos = numpy.arange(len(package_names))

    # plot high_counts bars for each package
    ax.barh(y_pos, high_counts, align='center')
    # plot medium_counts bars for each package, using high_counts as left limit
    ax.barh(y_pos, medium_counts, left=high_counts, align='center')
    # plot low_counts bars for each package, using high + medium counts as left limit
    high_plus_medium_counts = [h + m for h,m in zip(high_counts, medium_counts)]
    ax.barh(y_pos, low_counts, left=high_plus_medium_counts, color='yellow', align='center')

    # set ticks and labels
    ax.set_yticks(y_pos)
    ax.set_yticklabels(package_names)
    ax.invert_yaxis()  # labels read top-to-bottom
    ax.legend(labels=['High', 'Medium', 'Low'])

    plt.savefig(figname, format="pdf", bbox_inches="tight")
    plt.show()

def plot_old_new(intermediate_old_filename, intermediate_new_filename, exclude_packages):
    op, np = read_intermediate_old_new(intermediate_old_filename, intermediate_new_filename)
    fig, ax = plt.subplots(figsize=(8.27, 11.69))

    if exclude_packages:
        for e in exclude_packages:
            remove_package_by_name(op, e)
            remove_package_by_name(np, e)

    # data
    old_package_names = [p.name + " (old)" for p in op]
    new_package_names = [p.name + " (new)" for p in np]
    old_high_counts = [p.high_count for p in op]
    old_medium_counts = [p.medium_count for p in op]
    old_low_counts = [p.low_count for p in op]
    new_high_counts = [p.high_count for p in np]
    new_medium_counts = [p.medium_count for p in np]
    new_low_counts = [p.low_count for p in np]
    y_pos = numpy.arange(2 * len(old_package_names))

    # merge data from old and new packages into alternated-elements lists
    package_names = [x for y in zip(old_package_names, new_package_names) for x in y]
    high_counts = [x for y in zip(old_high_counts, new_high_counts) for x in y]
    medium_counts = [x for y in zip(old_medium_counts, new_medium_counts) for x in y]
    low_counts = [x for y in zip(old_low_counts, new_low_counts) for x in y]

    # plot high_counts bars for each package
    ax.barh(y_pos, high_counts, align='center')
    # plot medium_counts bars for each package, using high_counts as left limit
    ax.barh(y_pos, medium_counts, left=high_counts, align='center')
    # plot low_counts bars for each package, using high + medium counts as left limit
    high_plus_medium_counts = [h + m for h,m in zip(high_counts, medium_counts)]
    ax.barh(y_pos, low_counts, left=high_plus_medium_counts, color='yellow', align='center')

    # set ticks and labels
    ax.set_yticks(y_pos)
    ax.set_yticklabels(package_names)
    ax.xaxis.tick_top()
    ax.invert_yaxis()  # labels read top-to-bottom
    ax.legend(labels=['High', 'Medium', 'Low'])

    plt.savefig("diagram.pdf", format="pdf", bbox_inches="tight")
    plt.show()
