import argparse

def parse_command_line():
    description_ = """Exports LaTeX table and bar plot from cvescan reports (.csv).
                    The converter can be used to generate a LaTeX table and plot
                    the diagram comparing an older and a newer version of a cvescan
                    report, or even for a single report only.
                    Intermediate csv file(s) will be generated depending on the\
                    number of inputs. For a single input, the intermediate file is\
                    named intermediate_single.csv, otherwise a pair of intermediates\
                    is generated: intermediate_old.csv and intermediate_new.csv.\
                    Do NOT rename them. The LaTeX table is exported through a file\
                    named table.md. The bar plot is exported in svg format, named\
                    diagram.svg.
                    """
    parser = argparse.ArgumentParser(description=description_)
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--single", nargs=1,
                        help="path/to/report.csv (to be used if only one report\
                        must be converted)")
    group.add_argument("--old-new", nargs=2, help="path/to/old.csv and path/to/new.csv",
                        metavar=('OLD','NEW'))
    parser.add_argument("--intermediates-only", action='store_true',
                        help="Generate intermediate(s) and exit.")
    group.add_argument("--export-and-plot", choices=['single', 'both'],
                        help="Use pre-generated intermediate(s) to export a table\
                        and a bar plot. single: use intermediate_single.csv.\
                        both: use intermediate_old.csv and intermediate_new.csv")

    return parser.parse_args(), parser
